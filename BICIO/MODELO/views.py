from django.shortcuts import render
from django.views.generic import TemplateView
from suds.client import Client
import suds

import sys

#!/usr/bin/env python
# -*- coding: utf-8 -*-

class ModelWebService:

	def __init__(self):
		self.WSDL = 'http://webservicebicio.ngrok.com/ServiceSafetyEJBService/ServiceSafetyEJB?WSDL'
		self.enableWS = 0

	###################################################################################################################
	# GRAFICOS #
	###################################################################################################################

	# Model for safety by Ciclovia and Santiago
	def sumCarIncidentCiclovia(self,fromDateInput,untilDateInput,ciclovia):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			fromDate = self.parseDateSend(fromDateInput)
			untilDate = self.parseDateSend(untilDateInput)
			response_suds = client.service.sumCarIncidentByCiclovia(fromDate, untilDate, ciclovia)
			self.enableWS = 1
		except:
			response_suds = [50,50]
			self.enableWS = 0
		return response_suds
		#retorna un XML con la cantidad de incidentes delictuales

	def sumDelictualIncidentCiclovia(self,fromDateInput,untilDateInput,ciclovia):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			fromDate = self.parseDateSend(fromDateInput)
			untilDate = self.parseDateSend(untilDateInput)
			response_suds = client.service.sumDelictualIncidentByCiclovia(fromDate, untilDate, ciclovia)
			self.enableWS = 1
		except:
			response_suds = [50,50]
			self.enableWS = 0
		return response_suds

	def sumCarIncidentSantiago(self,fromDateInput,untilDateInput):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			fromDate = self.parseDateSend(fromDateInput)
			untilDate = self.parseDateSend(untilDateInput)
			response_suds = client.service.sumCarIncident(fromDate,untilDate)
			self.enableWS = 1
		except:
			response_suds = [50,50]
			self.enableWS = 0
		return response_suds

	def sumDelictualIncidentSantiago(self,fromDateInput,untilDateInput):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			fromDate = self.parseDateSend(fromDateInput)
			untilDate = self.parseDateSend(untilDateInput)
			response_suds = client.service.sumDelictualIncident(fromDate,untilDate)
			self.enableWS = 1
		except:
			response_suds = [50,50]
			self.enableWS = 0
		return response_suds

	# Model for Safety by Ciclovia
	def sumSafeByCiclovia(self,fromDateInput,untilDateInput,ciclovia):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			fromDate = self.parseDateSend(fromDateInput)
			untilDate = self.parseDateSend(untilDateInput)
			response_suds = client.service.sumSafeByCiclovia(fromDate, untilDate, ciclovia)
			self.enableWS = 1
		except:
			response_suds = [50,50]
			self.enableWS = 0
		return response_suds

	def sumUnsafeByCiclovia(self,fromDateInput,untilDateInput,ciclovia):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			fromDate = self.parseDateSend(fromDateInput)
			untilDate = self.parseDateSend(untilDateInput)
			response_suds = client.service.sumUnsafeByCiclovia(fromDate, untilDate, ciclovia)
			self.enableWS = 1
		except:
			response_suds = [50,50]	
			self.enableWS = 0
		return response_suds

	
	#Model for lineal chart comparator
	def positive(self,dateInput):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			selectDate = self.parseDateSend(dateInput)
			#selectDate = self.parseDateSend()
			response_suds = client.service.positive(selectDate)
			self.enableWS = 1
		except:
			response_suds = [65, 59, 80, 81, 56, 55, 40, 0 , 2, 6, 3, 6, 7,10, 3, 2, 0, 0, 0, 0, 0, 0, 0,24,25,20,30,28,29,30]
			self.enableWS = 0
		return response_suds

	def negative(self,dateInput):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			selectDate = self.parseDateSend(dateInput)
			#selectDate = self.parseDateSend(dateInput)
			response_suds = client.service.negative(selectDate)
			self.enableWS = 1
		except:
			response_suds = [28, 48, 40, 19, 15, 27, 7, 4, 2, 6, 3, 6, 7,10, 3, 2, 0, 0, 0, 0, 0, 0, 0,15,0,2,5,2,3,4]
			self.enableWS = 0
		return response_suds

	def neutral(self,dateInput):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			selectDate = self.parseDateSend(dateInput)
			#selectDate = self.parseDateSend()
			response_suds = client.service.neutral(selectDate)
			self.enableWS = 1
		except:
			response_suds = [50, 50, 15, 19, 15, 6, 7, 4, 1, 1, 4, 6, 1,1, 3, 2, 0, 0, 0, 0, 0, 0, 0,6,0,2,5,2,3,4]
			self.enableWS = 0
		return response_suds
	
	###################################################################################################################
	# MAPAS #
	###################################################################################################################

	# Map for distribution for types of incident by ciclovia in the interval of time fromDate and UntiDate
	def getListGeoCarIncident(self):
		try:
			reload(sys)
			sys.setdefaultencoding("utf-8")

			client = Client(self.WSDL)
			client.options.cache.clear()
			response_suds = client.service.carIncident7Days()

			myString = ''
			newTweet = 0
			sizeList = len(response_suds)
			for tweet in response_suds:
				if newTweet + 1 == sizeList:
					myString = myString+tweet
					newTweet+=1
				else:
					myString = myString+tweet+'&bicio'
					newTweet+=1
			response_suds = myString

			self.enableWS = 1
		except:
			response_suds = "-33.503576&bicio-70.757337&bicioPajaritos con chile&bicio0.75&bicio-33.497206&bicio-70.757573&bicioCerca de santiago buenas&bicio0.098"
			self.enableWS = 0
		return response_suds

	def getListGeoDelictualIncident(self):
		try:
			reload(sys)
			sys.setdefaultencoding("utf-8")

			client = Client(self.WSDL)
			client.options.cache.clear()
			response_suds = client.service.delictualIncident7Days()
			
			myString = ''
			newTweet = 0
			sizeList = len(response_suds)
			for tweet in response_suds:
				if newTweet + 1 == sizeList:
					myString = myString+tweet
					newTweet+=1
				else:
					myString = myString+tweet+'&bicio'
					newTweet+=1
			response_suds = myString
			self.enableWS = 1
		except:	
			response_suds = "-33.450071&bicio-70.642230&bicioEsta es de santa Isabel&bicio0.34&bicio-33.435427&bicio-70.604718&bicioEsta es de pocuro con lyon&bicio1"
			self.enableWS = 0
		return response_suds

	def geoSafe(self):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			response_suds = client.service.safe()
			self.enableWS = 1
		except:
			response_suds = "[-33.427933, -70.607566, -33.433662, -70.604322, -33.46207, -70.71933, -33.427956, -70.607561, -33.449774, -70.672525, -33.441859, -70.601686, -33.423819, -70.608966, -33.437326, -70.678923 , -33.424356, -70.608891, -33.443172, -70.603165 , -33.437326, -70.678923 , -33.452333, -70.691335, -33.440711, -70.632387, -33.437146, -70.675919, -33.453076, -70.687187 , -33.434564, -70.605016 , -33.429049, -70.585291, -33.467143, -70.732364, -33.444265, -70.631116, -33.434769, -70.60299, -33.451535, -70.678197, -33.437075, -70.675329, -33.448473, -70.602772, -33.452799, -70.679529, -33.453739, -70.690534, -33.424549, -70.608789, -33.452208, -70.683539, -33.425104, -70.609014, -33.435177, -70.665706, -33.442694, -70.632735, -33.434502, -70.587568, -33.431413, -70.585385, -33.451911, -70.660916, -33.457275, -70.688325, -33.433214, -70.608988, -33.444635, -70.631064, -33.435917, -70.603956, -33.428222, -70.589622, -33.439037, -70.656207, -33.436523, -70.602115, -33.439170, -70.603315 , -33.436507, -70.603287, -33.43816, -70.633637, -33.43824, -70.632676]"
			self.enableWS = 0
		return response_suds

	def geoUnsafe(self):
		try:
			client = Client(self.WSDL)
			client.options.cache.clear()
			response_suds = client.service.unsafe()
			self.enableWS = 1			
		except:
			response_suds = "[-33.475232, -70.740169, -33.447322, -70.657884, -33.444087, -70.66974, -33.434206, -70.587004, -33.43989, -70.656098, -33.435598, -70.599704, -33.434723, -70.601368, -33.436232, -70.672569, -33.435742, -70.669727, -33.426459, -70.606841, -33.448304, -70.602141, -33.438409, -70.633393, -33.475232, -70.740169, -33.425425, -70.580638, -33.425433, -70.58064, -33.425426, -70.580644, -33.425432, -70.580644, -33.425428, -70.580633, -33.475232, -70.740169, -33.425423, -70.580641, -33.425435, -70.580634, -33.438151, -70.632663, -33.425412, -70.580634, -33.425422, -70.580641, -33.425431, -70.580646, -33.475232, -70.740169, -33.436251, -70.63178, -33.432381, -70.584605, -33.436035, -70.608199, -33.454519, -70.59555, -33.456839, -70.594269, -33.475232, -70.740169, -33.438063, -70.666753, -33.4365, -70.609821, -33.475232, -70.740169, -33.475232, -70.740169, -33.321548, -70.691386, -33.448585, -70.671885, -33.425691, -70.60998, -33.436242, -70.672237, -33.437093, -70.675844, -33.448746, -70.672884, -33.437805, -70.611992, -33.436053, -70.673031, -33.430389, -70.585842, -33.436142, -70.67254, -33.432003, -70.584875, -33.427525, -70.591188, -33.41295, -70.599359, -33.475232, -70.740169, -33.438353, -70.633511, -33.450366, -70.639677, -33.422963, -70.609794, -33.436034, -70.595773, -33.438399, -70.633497, -33.437458, -70.633606, -33.428591, -70.607755, -33.438343, -70.633539, -33.437084, -70.679030]"
			self.enableWS = 0
		return response_suds

	# parse date input in a sequence of number example 2014-09-30 --> 20140930
	def parseDateSend(self,date):
		parseDate = ""
		for char in date:
			if char != "-":
				parseDate = parseDate+char
		print parseDate
		return parseDate

	#Getters and setters
	def getEnableWS(self):
		return self.enableWS


class llamadaEjemplos(TemplateView):

	def __init__(self,template):
		self.template = template

	def goTemplate(self,request):

		return render(request,self.template,{})

