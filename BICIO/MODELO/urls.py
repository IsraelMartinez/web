from django.conf.urls import patterns, include, url
from django.contrib import admin
#from ejemplo import views
from .views import llamadaEjemplos

#llamadaEjemplos_ = llamadaEjemplos('Hola','Mundo')
#llamadaEjemplos = llamadaEjemplos.as_view()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BICIO.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r'^llamadaEjemplo_1/$', llamadaEjemplos('ejemplo/template_1.html').goTemplate),
	url(r'^llamadaEjemplos/$', llamadaEjemplos('ejemplo/template_2.html').goTemplate),
	url(r'^datepickerFromUntilDate/$', llamadaEjemplos('ejemplo/datepickerFromUntilDate.html').goTemplate),
	url(r'^displayMap/$',llamadaEjemplos('ejemplo/displayMap.html').goTemplate),
	url(r'^helloMap/$',llamadaEjemplos('ejemplo/helloMap.html').goTemplate),
)