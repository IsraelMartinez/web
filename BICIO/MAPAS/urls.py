from django.conf.urls import patterns, include, url
from django.contrib import admin
#from ejemplo import views
from .views import MAPS

#llamadaEjemplos_ = llamadaEjemplos('Hola','Mundo')
#llamadaEjemplos = llamadaEjemplos.as_view()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BICIO.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	#url(r'^disFreqIncidentCiclovia/$', Graphics('init').disFreqIncidentCiclovia),
	#url(r'^disFreqIncidentCicloviaForm/$', Graphics('init').disFreqIncidentCicloviaForm),
	url(r'^sevenDays/$', MAPS('MAPAS/sevenDays/sevenDays.html').displayMap),
	url(r'^sevenDaysHeatMap/$', MAPS('MAPAS/sevenDays/sevenDaysHeatMap.html').displayHeatMap),
	url(r'^heatmap/$', MAPS('MAPAS/sevenDays/heatmap.html').goTemplate),
)