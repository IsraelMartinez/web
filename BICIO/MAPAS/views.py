from django.shortcuts import render
from django.views.generic import TemplateView
import suds
import json
from MODELO.views import ModelWebService
#!/usr/bin/env python
# -*- coding: utf-8 -*-

class MAPS(TemplateView):

	def __init__(self,template):
		self.template = template

	# For sevenDays
	def displayMap(self,request):
		ws =  ModelWebService()
		listGeo1 = ws.getListGeoCarIncident()
		listGeo2 = ws.getListGeoDelictualIncident()
		enableWS = ws.getEnableWS()
		context = {'listGeo1':listGeo1,'listGeo2':listGeo2,'enableWS':enableWS}

		return render(request,self.template,context)

	def displayHeatMap(self,request):
		ws =  ModelWebService()
		listGeo1 = ws.geoSafe()
		listGeo2 = ws.geoUnsafe()
		enableWS = ws.getEnableWS()
		context = {'listGeo1':listGeo1,'listGeo2':listGeo2,'enableWS':enableWS}		

		return render(request,self.template,context)

	def goTemplate(self,request):
		return render(request,self.template,{})
