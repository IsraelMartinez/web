from django.shortcuts import render
from django.views.generic import TemplateView
import suds
from MODELO.views import ModelWebService

#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Graphics(TemplateView):
	
	def __init__(self,template,byDefault):
		self.template = template
		self.byDefault = byDefault

	#For Frequen Incident for Ciclovia and Santiago
	def disFreqIncidentCicloviaForm(self,request):
		ws = ModelWebService()
		ciclovia = request.GET.get('ciclovia')
		fromDate = request.GET.get('fromDate')
		untilDate = request.GET.get('untilDate')
		sumCar = ws.sumCarIncidentCiclovia(fromDate,untilDate,ciclovia)
		sumDel = ws.sumDelictualIncidentCiclovia(fromDate,untilDate,ciclovia)
		total = sumCar[1]+sumDel[1]
		enableWS = ws.getEnableWS()
		context = {'fromDate': fromDate,'untilDate': untilDate,'sumCar': sumCar[0],'sumDel': sumDel[0],'ciclovia': ciclovia,'total': total,'enableWS':enableWS}

		return render(request,'GRAFICOS/moreFreqIncidents/disFreqIncidentCicloviaForm.html', context)

	def disFreqIncidentCiclovia(self,request):
		ws = ModelWebService()
		ciclovia = request.GET.get('ciclovia')
		fromDate = request.GET.get('fromDate')
		untilDate = request.GET.get('untilDate')
		sumCar = ws.sumCarIncidentCiclovia(fromDate,untilDate,ciclovia)
		sumDel = ws.sumDelictualIncidentCiclovia(fromDate,untilDate,ciclovia)
		total = sumCar[1]+sumDel[1]
		enableWS = ws.getEnableWS()
		context = {'fromDate': fromDate,'untilDate': untilDate,'sumCar': sumCar[0],'sumDel': sumDel[0],'ciclovia': ciclovia,'total': total,'enableWS':enableWS}

		return render(request,self.template,context)

	def disFreqIncidentSantiago(self,request):
		ws = ModelWebService()
		fromDate = request.GET.get('fromDate')
		untilDate = request.GET.get('untilDate')
		sumCar = ws.sumCarIncidentSantiago(fromDate,untilDate)
		sumDel = ws.sumDelictualIncidentSantiago(fromDate,untilDate)
		total = sumCar[1]+sumDel[1]
		enableWS = ws.getEnableWS()
		context = {'fromDate': fromDate,'untilDate': untilDate,'sumCar': sumCar[0],'sumDel': sumDel[0],'total': total,'enableWS':enableWS}

		return render(request,self.template,context)

	#For Safety Ciclovia
	def disSafetyCicloviaDefault(self,request):
		ws = ModelWebService()
		ciclovia = self.byDefault
		fromDate = request.GET.get('fromDate')
		untilDate = request.GET.get('untilDate')
		sumSafe = ws.sumSafeByCiclovia(fromDate,untilDate,ciclovia)
		sumUnsafe = ws.sumUnsafeByCiclovia(fromDate,untilDate,ciclovia)
		total = sumSafe[1]+sumUnsafe[1]
		enableWS = ws.getEnableWS()
		context = {'fromDate': fromDate,'untilDate': untilDate,'sumSafe': sumSafe[0],'sumUnsafe': sumUnsafe[0],'ciclovia':ciclovia,'total': total,'enableWS':enableWS}

		return render(request,self.template,context)

	def disSafetyCiclovia(self,request):
		ws = ModelWebService()
		ciclovia = request.GET.get('ciclovia')
		fromDate = request.GET.get('fromDate')
		untilDate = request.GET.get('untilDate')
		sumSafe = ws.sumSafeByCiclovia(fromDate,untilDate,ciclovia)
		sumUnsafe = ws.sumUnsafeByCiclovia(fromDate,untilDate,ciclovia)
		total = sumSafe[1]+sumUnsafe[1]
		enableWS = ws.getEnableWS()
		context = {'fromDate': fromDate,'untilDate': untilDate,'sumSafe': sumSafe[0],'sumUnsafe': sumUnsafe[0],'ciclovia':ciclovia,'total': total,'enableWS':enableWS}

		return render(request,self.template,context)

	def approvalEvent(self,request):
		ws = ModelWebService()
		fromDate = request.GET.get('fromDate')
		data1 = request.GET.get('data1')
		data2 = request.GET.get('data2')
		data3 = request.GET.get('data3')
		notice = request.GET.get('notice')
		arrayData1 = ws.positive(fromDate)
		arrayData2 = ws.negative(fromDate)
		arrayData3 = ws.neutral(fromDate)
		enableWS = ws.getEnableWS()
		context = {'fromDate':fromDate,'arrayData1':arrayData1,'arrayData2':arrayData2,'arrayData3':arrayData3,'data1':data1,'data2':data2,'data3':data3,'notice':notice,'enableWS':enableWS}

		return render(request,self.template,context)

	def getOneDate(self,request):
		fromDate = request.GET.get('fromDate')
		data1 = request.GET.get('data1')
		data2 = request.GET.get('data2')
		data3 = request.GET.get('data3')
		notice = request.GET.get('notice')
		context = {'fromDate':fromDate,'data1':data1,'data2':data2,'data3':data3,'notice':notice}

		return render(request,self.template,context)

	def getDate(self,request):
		fromDate = request.GET.get('fromDate')
		untilDate = request.GET.get('untilDate')
		context = {'fromDate':fromDate,'untilDate':untilDate}

		return render(request,self.template,context)

	def goTemplate(self,request):
		return render(request,self.template,{})