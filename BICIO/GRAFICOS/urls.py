from django.conf.urls import patterns, include, url
from django.contrib import admin
#from ejemplo import views
from .views import Graphics

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BICIO.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
     #url(r'^MYURL/$', MYCLASE('Template a cargar','Template destino despues del formulario').goDateForm),

    #Urls for more frequent type incident (ciclovia and Santiago)

    url(r'^menuIncidentCicloviaSantiago/$', Graphics('GRAFICOS/moreFreqIncidents/menuIncidentCicloviaSantiago.html','destiny').getDate),
    url(r'^disFreqIncidentCicloviaForm/$', Graphics('GRAFICOS/moreFreqIncidents/disFreqIncidentCicloviaForm.html','destiny').goTemplate),
	url(r'^disFreqIncidentCiclovia/$', Graphics('GRAFICOS/moreFreqIncidents/disFreqIncidentCiclovia.html','destiny').disFreqIncidentCiclovia),
	url(r'^disFreqIncidentSantiagoForm/$', Graphics('GRAFICOS/moreFreqIncidents/disFreqIncidentSantiagoForm.html','destiny').goTemplate),
	url(r'^disFreqIncidentSantiago/$', Graphics('GRAFICOS/moreFreqIncidents/disFreqIncidentSantiago.html','destiny').disFreqIncidentSantiago),
	url(r'^disIncidentCSDateForm/$', Graphics('GRAFICOS/moreFreqIncidents/disIncidentCicloviaSantiagoDateForm.html','destiny').getDate),
    url(r'^disSafetyCicloviaDateForm/$', Graphics('GRAFICOS/safetyCiclovias/disSafetyCicloviaDateForm.html','destiny').getDate),
    url(r'^disSafetyCicloviaDefault/$', Graphics('GRAFICOS/safetyCiclovias/disSafetyCiclovia.html','lyon').disSafetyCicloviaDefault),
    url(r'^disSafetyCiclovia/$', Graphics('GRAFICOS/safetyCiclovias/disSafetyCiclovia.html','destiny').disSafetyCiclovia),
    url(r'^approvalEventForm/$', Graphics('GRAFICOS/approvalEvent/approvalEventForm.html','destiny').getOneDate),
    url(r'^approvalEvent/$', Graphics('GRAFICOS/approvalEvent/approvalEvent.html','destiny').approvalEvent),


)